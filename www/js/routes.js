App.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider

    // setup an abstract state for the tabs directive
    .state('tab', {
        url: "/tab",
        abstract: true,
        templateUrl: "templates/tabs.html"
    })

    // Each tab has its own nav history stack:
    .state('landing', {
        url: '/landing',
        templateUrl: 'templates/landing.html',
        controller: 'LandingCtrl'
    })

    .state('disclaimer', {
        url: '/disclaimer',
        templateUrl: 'templates/disclaimer.html',
        controller: 'DisclaimerCtrl'
    })

    .state('login', {
        url: '/login',
        templateUrl: 'templates/login.html',
        controller: 'LoginCtrl'
    })

    .state('logout', {
        url: '/logout',
        controller: 'LogoutCtrl'
    })

    .state('forgot-password', {
        url: '/forgot',
        templateUrl: 'templates/forgot-password.html',
        controller: 'ForgotPasswordCtrl'
    })

    .state('password-confirm', {
        url: '/password-confirm',
        templateUrl: 'templates/password-confirm.html'
    })

    .state('signup', {
        url: '/signup',
        templateUrl: 'templates/signup.html',
        controller: 'SignupCtrl'
    })

    .state('create-profile', {
        url: '/create-profile',
        templateUrl: 'templates/create-profile.html',
        controller: 'CreateProfileCtrl'
    })

    .state('tab.profiles', {
        url: '/profiles',
        views: {
            'tab-profiles': {
                templateUrl: 'templates/tab-profiles.html',
                controller: 'ProfilesCtrl'
            }
        }
    })

    .state('profile-detail', {
        url: '/tab/profile/:profileId',
        views: {
            'tab-profiles': {
                templateUrl: 'templates/profile-detail.html',
                controller: 'ProfileDetailCtrl'
            }
        }
    })

    .state('results', {
        url: '/results',
        templateUrl: 'templates/results.html',
        controller: 'ResultsCtrl'
    })


     .state('scan', {
         url: '/scan',
         templateUrl: 'templates/tab-scan.html',
         controller: 'ScanCtrl'
     })

    .state('tab.settings', {
        url: '/settings',
        views: {
            'tab-settings': {
                templateUrl: 'templates/tab-settings.html',
                controller: 'SettingsCtrl'
            }
        }
    })

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('landing');
});