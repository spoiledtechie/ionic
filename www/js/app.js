var App = angular.module('FoodAllergyWizard', [
	'ionic',
	'FoodAllergyWizard.Configs',
	'FoodAllergyWizard.controllers',
	'FoodAllergyWizard.services',
	'ngSanitize',
	'ngResource',
	'FoodAllergyWizard.Resource.Users',
	'FoodAllergyWizard.Resource.Allergens'
])

App.run(function($ionicPlatform) {
	$ionicPlatform.ready(function() {
		// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard for form inputs)
		if (window.cordova && window.cordova.plugins.Keyboard) {

				cordova.plugins.Keyboard.close();
			cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
		}
		if (window.StatusBar) {

			// org.apache.cordova.statusbar required
			StatusBar.styleDefault();
		}

		if (window.cordova) {

			//launch instabug
			cordova.plugins.instabug.activate(
				{
					android: '605c9794795a121ca5b10eae2967b743',
					ios: '605c9794795a121ca5b10eae2967b743'
				},
				'shake',
				{
					commentRequired: true,
					colorTheme: 'dark',
					shakingThresholdAndroid: '0.1',
					shakingThresholdIPhone: '1.5',
					shakingThresholdIPad: '0.6',
					enableIntroDialog: false
				},
				function () {
					console.log('Instabug initialized.');
				},
				function (error) {
					console.log('Instabug could not be initialized - ' + error);
				}
			);

			//get preferences, current user, if any
			var prefs = plugins.appPreferences;
			prefs.fetch('current_user_id').then(function(value) {
				window.localStorage['current_user_id']=value;
			}, function(error) {
				console.log("Couldn't get preferences");
			});
		}

	});
});
