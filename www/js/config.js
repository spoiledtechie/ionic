angular.module('FoodAllergyWizard.Configs', [])

.constant('CONFIG', {
	//rootUrl: 'http://allergy.dev.nrh-dev.com',
	rootUrl: 'http://foodallergywizard.com/api',
	apiServer: '/services',
	apiKey: 'pcthes2k26uq3sscb7jsa4t6',
	foodApiUrl: 'http://api.foodessentials.com/createsession?uid=demoUID_01&devid=demoDev_01&appid=pcthes2k26uq3sscb7jsa4t6&f=json&c=JSON_CALLBACK&&api_key=pcthes2k26uq3sscb7jsa4t6'
})

// setup allergens list from Food Essential API
.constant('ALLERGENS', [ 
	{
		allergen: 'Cereals',
		related: null
	},
	{
		allergen: 'Shellfish',
		related: 'Fish'
	},
	{
		allergen: 'Egg',
		related: null
	},
	{
		allergen: 'Fish',
		related: 'Shellfish'
	},
	{
		allergen: 'Milk',
		related: null
	},
	{
		allergen: 'Peanuts',
		related: 'Tree Nuts'
	},
	{
		allergen: 'Sulfites',
		related: null
	},
	{
		allergen: 'Tree Nuts',
		related: 'Coconut'
	},
	{
		allergen: 'Soybean',
		related: null
	},
	{
		allergen: 'Sesame Seeds',
		related: null
	},
	{
		allergen: 'Gluten',
		related: null
	},
	{
		allergen: 'Lactose',
		related: null
	},
	{
		allergen: 'Corn',
		related: null
	},
	{
		allergen: 'Wheat',
		related: null
	},
	{
		allergen: 'Coconut',
		related: null
	}
])
;