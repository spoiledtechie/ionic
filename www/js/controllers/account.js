Controllers

.controller('LoginCtrl', function($scope, $location, $http, $rootScope, Users, Util, $ionicPopup, CONFIG) {
	$scope.authenticateUser = function(info) {
		// Form data
		var username = info.username;
		var password = info.password;

		$scope.showAlert = function() {
			var alertPopup = $ionicPopup.alert({
				title: 'Login failed',
				template: 'Username or password is incorrect. Please try again.',
				okType: 'button-royal'
			});
		};

		// Login Steps + POST request
		$scope.isDisabled = true;

        $http.post(CONFIG.rootUrl + CONFIG.apiServer + '/user/authenticate', {
            "user_info": {
                "username": username,
                "password": password
            }
        }).success(function(data) {
			if (typeof(data.uid) !== 'undefined') {
				Util.setCurrentUserId(data.uid);
				var current_user_id = Util.currentUserId();
				$location.path('/tab/profiles');
			} else {
				$scope.notice = "Invalid username or password.";
				$scope.isDisabled = false;
			}
		}, function(error) {
			$scope.showAlert();
			$scope.isDisabled = false;
		});
	}
})

.controller('LogoutCtrl', function($scope, $location, $window, $rootScope, Users, Util) {
	Util.setCurrentUserId(null);
	location.href = '#/login';
})

.controller('SignupCtrl', function($scope, $location, $http, Users, Util, $ionicPopup,CONFIG) {
	$scope.newUser = {};
	$scope.isDisabled = false;

	$scope.signupUser = function(info) {
		$scope.isDisabled = true;
		var data_to_send = JSON.stringify({
			"user_info": {
				username: $scope.newUser.username,
				firstname: $scope.newUser.firstname,
				lastname: $scope.newUser.lastname,
				email: $scope.newUser.email,
				password: $scope.newUser.password,
				password_confirm: $scope.newUser.password_confirm
			}
		});

		// validate data before send
		if (($scope.newUser.firstname === undefined) || ($scope.newUser.lastname === undefined) || ($scope.newUser.email === undefined) || ($scope.newUser.password === undefined) || ($scope.newUser.password_confirm === undefined)) {
			$ionicPopup.alert({
				title: 'Sign Up Failed',
				template: 'All fields are required.',
				okType: 'button-royal'
			});
			$scope.isDisabled = false;
		} else if ($scope.newUser.password != $scope.newUser.password_confirm) {
			$ionicPopup.alert({
				title: 'Sign Up Failed',
				template: 'Password and Confirm Password must match.',
				okType: 'button-royal'
			});
			$scope.isDisabled = false;
		} else {
            $http.post(CONFIG.rootUrl + CONFIG.apiServer + '/user/create', data_to_send)
                .success(function(data, status, headers, config) {
				$scope.isDisabled = false;
				console.log(data)
				if (typeof(data.uid) !== 'undefined') {
					// auto login to create profile
                    $http.post(CONFIG.rootUrl + CONFIG.apiServer + '/user/authenticate', {
                        "user_info": {
                            "username": $scope.newUser.username,
                            "password": $scope.newUser.password
                        }
                    }).success(function(data) {
						Util.setCurrentUserId(data.uid);
						$location.path('/create-profile');
					}, function(data) {
						console.log(data)
					})
				} else {
					$ionicPopup.alert({
						title: 'Sign Up Failed',
						template: 'Username or email address is already in use',
						okType: 'button-royal'
					});
					$scope.isDisabled = false;
				}
			}, function(data, status, headers, config) {
				console.log("Error Occurred:", data);
				$scope.isDisabled = false;
			});
		}
	};

	$scope.cancelSignup = function() {
		$scope.isDisabled = false;
		$location.path('/login');
	};
})

.controller('ForgotPasswordCtrl', function($scope, $location, $http, CONFIG) {
	$scope.resetPassword = function(email) {
		$scope.isDisabled = true;
		$http.get(CONFIG.rootUrl + CONFIG.apiServer + '/reset_password/' + email)
		.success(function(data){
			$scope.isDisabled = false;
			$location.path('password-confirm');
		});
	};
})
