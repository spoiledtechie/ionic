Controllers

.controller('SettingsCtrl', function($scope, $location, $http, Users, Util, SocialSharing, $ionicPopup, CONFIG) {
	$scope.gotoProfiles = function() {
		$location.path('/tab/profiles');
	};

	// get user profile 
	$scope.info = {};

    $http.get(CONFIG.rootUrl + CONFIG.apiServer + '/user/' +Util.currentUserId() , {
        userId: Util.currentUserId()
    }).success(function(data) {
		$scope.info = angular.copy(data);
	}, function(data) {
		console.log('Error:', data);
	});

	$scope.updateUser = function() {
		var username = $scope.editInfo.name;
		var firstname = $scope.editInfo.first_name;
		var lastname = $scope.editInfo.last_name;
		var email = $scope.editInfo.mail;
		var password = $scope.editInfo.password;
		var password_confirm = $scope.editInfo.password_confirm;

		if(password != password_confirm) {
			$ionicPopup.alert({
				title: 'Password must match',
				template: 'Password confirm must match',
				okType: 'button-royal'
			});

			return false;
		}

		// Preparing to convert data to correct JSON
		if (password != '' && password == password_confirm) {
			var data_to_send = JSON.stringify({
				"user_info": {
					uid: Util.currentUserId(),
					username: username,
					firstname: firstname,
					lastname: lastname,
					email: email,
					password: password
				}
			});
		} else {
			var data_to_send = JSON.stringify({
				"user_info": {
					uid: Util.currentUserId(),
					username: username,
					firstname: firstname,
					lastname: lastname,
					email: email
				}
			});
		}

		// Signup Steps + POST request
		$scope.isDisabled = true;
        $http.post(CONFIG.rootUrl + CONFIG.apiServer + '/user/' +Util.currentUserId() +'/update' ,data_to_send)
			.success(function(data) {
			if (data.uid) {
				$scope.info = $scope.editInfo;
				$scope.editing = false;

				delete $scope.info.password;
				delete $scope.info.password_confirm;
			}
			$scope.isDisabled = false;
		}, function(data) {
			$scope.isDisabled = false;
			console.log("Error Occurred:", data);
		});
	}

	$scope.twitterShare = function() {
		var message = 'I got help shopping for allergens with this amazing app called the Food Allergy Wizard';
		var file = 'www/img/social-share.png';
		var link = '';
		SocialSharing.shareViaTwitter(message, file, link).then(function(result) {
			// Success! 
			console.log(result);
		}, function(err) {
			// An error occured. Show a message to the user
			console.log(err);
		});
	};

	$scope.facebookShare = function() {
		var message = 'I got help shopping for allergens with this amazing app called the Food Allergy Wizard';
		var file = 'www/img/social-share.png';
		var link = '';
		SocialSharing.shareViaFacebook(message, file, link).then(function(result) {
			// Success! 
			console.log(result);
		}, function(err) {
			// An error occured. Show a message to the user
			console.log(err);
		});
	};

	$scope.editing = false;
	$scope.editSettings = function() {
		$scope.editInfo = angular.copy($scope.info);
		$scope.editing = true;
	};
	$scope.cancelEdit = function() {
		$scope.editInfo = {};
		$scope.editing = false;
	};
})