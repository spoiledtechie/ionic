var Controllers = angular.module('FoodAllergyWizard.controllers', []);

Controllers

/*.config(function($httpProvider) {
	//Enable cross domain calls
	$httpProvider.defaults.useXDomain = true;

	//Remove the header used to identify ajax call  that would prevent CORS from working
	delete $httpProvider.defaults.headers.common['X-Requested-With'];
})
*/
.controller('LandingCtrl', function($scope, $state, $timeout, $ionicPlatform) {
	var promise = $timeout(function() {
			$state.go('disclaimer');
	}, 1500);
	$scope.goAhead = function() {
		$state.go('disclaimer');
		$timeout.cancel(promise);
	};
})

.controller('DisclaimerCtrl', function($scope, $ionicScrollDelegate, $state, Util) {
	$scope.acceptDisclaimer = function() {

		var current_user_id = Util.currentUserId();
		if (current_user_id && current_user_id != null && current_user_id != "null") {
			console.log("Got user " + current_user_id);
			$state.go('tab.profiles')
		}
		else {
			console.log("No current user");
			$state.go('login');
		}
	}
})

;
