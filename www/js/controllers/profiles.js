Controllers

.controller('CreateProfileCtrl', function ($scope, $q, $http, $location, $ionicPopup, $ionicModal, NativeCamera, Users, Util, Allergens, CONFIG) {
    $scope.userId = Util.currentUserId();
    $scope.newProfile = {};
    $scope.newProfile.allergens = [];
    $scope.display_name = Util.currentUserDisplayName();
    $scope.allergens = [];
    $scope.enabledAllergens = [];
    $scope.allergensBtn = [];
    $scope.allergensDropdown = [];
    $scope.product_name = '';
    $scope.product_warning = '';
    $scope.allergen_list = '';
    $scope.allergen_list_may_contain = '';
    Allergens.get().$promise.then(function (data) {
        var allergensFromServer = data.allergens;
        angular.forEach(allergensFromServer, function (a) {
            if ((a.disable === null) || (a.disable == '0') || (a.disable === 0)) {
                $scope.allergens.push(a);
                $scope.enabledAllergens.push(a.allergen);
                $scope.allergensDropdown.push(a.allergen);
            }
        });

        angular.forEach($scope.enabledAllergens, function (a, i) {
            if (i <= 9) {
                $scope.allergensBtn.push(a);
            }
        });

        for (var key in $scope.allergensBtn) {
            var value = $scope.allergensBtn[key];
            var index = $scope.allergensDropdown.indexOf(value);
            if (index != -1) {
                $scope.allergensDropdown.splice(index, 1);
            }
        }
    }, function (error) {
        console.log(error);
    });

    // Steps to add profiles
    $scope.goToStep2 = function () {
        if (angular.isUndefined($scope.newProfile.firstName) || angular.isUndefined($scope.newProfile.lastName)) {
            $ionicPopup.alert({
                title: "Missing Information",
                template: 'First name and last name are required'
            });
        } else {
            $scope.openStep2();
        }
    };

    $scope.goToStep22 = function () {
        $scope.openStep22();
    };
    $scope.goToStep23 = function () {
        $scope.openStep23();
    };

    $scope.backToStep2 = function () {
        $scope.openStep2();
        if ($scope.modalStep22) {
            $scope.closeStep22();
        } else {
            $scope.closeStep23();
        }
    }

    $scope.goToStep3 = function () {
        $scope.openStep3();
        $scope.closeStep2();
        if ($scope.modalStep22) {
            $scope.closeStep22();
        } else {
            $scope.closeStep23();
        }
    };

    $scope.addMoreAllergen = function (relatedAllergen) {
        // push the allergen list
        $scope.newProfile.allergens.push(relatedAllergen);
        //reset
        $scope.selectedAllergen = '';
        $scope.relatedAllergen = '';
        $scope.closeModalNewAllergen(); // close modal
    };

    $scope.showConfirmModal = function () {
        $scope.allergensSelected = [];
        for (var key in $scope.allergenInfo) {
            if ($scope.allergenInfo[key] == true) {
                $scope.allergensSelected.push(key);
            }
        }
        $scope.openConfirm();
    };

    $scope.closeConfirmModal = function () {
        $scope.closeConfirm();
    };

    $scope.createProfile = function () {
        // save profile to user then go to profile
        var data_to_send = JSON.stringify({
            "profile_info": {
                allergens: $scope.newProfile.allergens,
                firstname: $scope.newProfile.firstName || '',
                lastname: $scope.newProfile.lastName || '',
                image: $scope.newProfile.image
            }
        });

        $scope.isDisabled = true;
        $http.post(CONFIG.rootUrl + CONFIG.apiServer + '/user/' +Util.currentUserId() +'/family_member/add' ,data_to_send)
            .success(function () {
            $scope.closeStep3();
            $scope.closeConfirm();
            $scope.isDisabled = false;
            $location.path('/tab/profiles');
        }, function () {
            $scope.isDisabled = false;
            $location.path('/create-profile');
        });
    };

    $scope.choosePhoto = function () {
        NativeCamera.getPicture({
            allowEdit: true,
            correctOrientation: true,
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
            targetHeight: 320,
            targetWidth: 320
        }).then(function (imageURI) {
            $scope.newProfile.image = 'data:image/jpg;base64,' + imageURI;
            $scope.openStep22();
        }, function (err) {
            console.err(err);
        });
    }

    $scope.takePhoto = function () {
        NativeCamera.getPicture({
            quality: 100,
            destinationType: Camera.DestinationType.DATA_URL,
            allowEdit: true,
            targetWidth: 320,
            targetHeight: 320,
            saveToPhotoAlbum: false
        }).then(function (imageURI) {
            $scope.newProfile.image = 'data:image/jpg;base64,' + imageURI;
            $scope.openStep23();
        }, function (err) {
            console.err(err);
        });
    }

    $scope.toggleAllergen = function (allergen) {
        var idx = $scope.newProfile.allergens.indexOf(allergen);
        if (idx > -1) { // is currently selected
            $scope.newProfile.allergens.splice(idx, 1);
        } else { // is newly selected
            $scope.newProfile.allergens.push(allergen);
            angular.forEach($scope.allergens, function (a) {
                if (a.allergen == allergen) {
                    if (a.related && $scope.newProfile.allergens.indexOf(a.related) < 0 && $scope.enabledAllergens.indexOf(a.related) > 0) {
                        $scope.selectedAllergen = a.allergen;
                        $scope.relatedAllergen = a.related;
                        $scope.openModalNewAllergen();
                    }
                }
            });
        }
    };

    // Create and load the Modal
    var initStep2 = function () {
        if ($scope.modalStep2) {
            return $q.when();
        } else {
            return $ionicModal.fromTemplateUrl('step2.html', {
                scope: $scope,
                animation: 'slide-in-right'
            }).then(function (modal) {
                $scope.modalStep2 = modal;
            });
        }
    };
    $scope.openStep2 = function () {
        initStep2().then(function () {
            $scope.modalStep2.show();
        });
    };
    $scope.closeStep2 = function () {
        $scope.modalStep2.remove().then(function () {
            $scope.modalStep2 = null;
        });
    };

    var initStep22 = function () {
        if ($scope.modalStep22) {
            return $q.when();
        } else {
            return $ionicModal.fromTemplateUrl('step22.html', {
                scope: $scope,
                animation: 'slide-in-right'
            }).then(function (modal) {
                $scope.modalStep22 = modal;
            });
        }
    };
    $scope.openStep22 = function () {
        initStep22().then(function () {
            $scope.modalStep22.show();
        });
    };
    $scope.closeStep22 = function () {
        $scope.modalStep22.remove().then(function () {
            $scope.modalStep22 = null;
        });
    };

    var initStep23 = function () {
        if ($scope.modalStep23) {
            return $q.when();
        } else {
            return $ionicModal.fromTemplateUrl('step23.html', {
                scope: $scope,
                animation: 'slide-in-right'
            }).then(function (modal) {
                $scope.modalStep23 = modal;
            });
        }
    };
    $scope.openStep23 = function () {
        initStep23().then(function () {
            $scope.modalStep23.show();
        });
    };
    $scope.closeStep23 = function () {
        $scope.modalStep23.remove().then(function () {
            $scope.modalStep23 = null;
        });
    };

    var initStep3 = function () {
        if ($scope.modalStep3) {
            return $q.when();
        } else {
            return $ionicModal.fromTemplateUrl('step3.html', {
                scope: $scope,
                animation: 'slide-in-right'
            }).then(function (modal) {
                $scope.modalStep3 = modal;
            });
        }
    };
    $scope.openStep3 = function () {
        initStep3().then(function () {
            $scope.modalStep3.show();
        });
    };
    $scope.closeStep3 = function () {
        $scope.modalStep3.remove().then(function () {
            $scope.modalStep3 = null;
        });
    };

    var initModalMoreAllergens = function () {
        if ($scope.modalMoreAllergens) {
            return $q.when();
        } else {
            return $ionicModal.fromTemplateUrl('more-allergens-modal.html', {
                scope: $scope,
                animation: 'slide-in-right'
            }).then(function (modal) {
                $scope.modalMoreAllergens = modal;
            });
        }
    };
    $scope.openModalMoreAllergens = function () {
        initModalMoreAllergens().then(function () {
            $scope.modalMoreAllergens.show();
        });
    };
    $scope.closeModalMoreAllergens = function () {
        $scope.modalMoreAllergens.remove().then(function () {
            $scope.modalMoreAllergens = null;
        });
    };

    var initModalNewAllergen = function () {
        if ($scope.modmodalNewAllergenal) {
            return $q.when();
        } else {
            return $ionicModal.fromTemplateUrl('new-allergen-modal.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal) {
                $scope.modalNewAllergen = modal;
            });
        }
    };
    $scope.openModalNewAllergen = function () {
        initModalNewAllergen().then(function () {
            $scope.modalNewAllergen.show();
        });
    };
    $scope.closeModalNewAllergen = function () {
        $scope.modalNewAllergen.remove().then(function () {
            $scope.modalNewAllergen = null;
        });
    };

    var initConfirm = function () {
        if ($scope.modalConfirm) {
            return $q.when();
        } else {
            return $ionicModal.fromTemplateUrl('confirm-modal.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal) {
                $scope.modalConfirm = modal;
            });
        }
    };
    $scope.openConfirm = function () {
        initConfirm().then(function () {
            $scope.modalConfirm.show();
        });
    };
    $scope.closeConfirm = function () {
        $scope.modalConfirm.remove().then(function () {
            $scope.modalConfirm = null;
        });
    };
})

.controller('ProfilesCtrl', function ($scope, $q, $state, $ionicModal, $ionicPopup, $http, $location, BarcodeScanner, NativeCamera, Users, Util, Allergens, $timeout, CONFIG) {
    // global vars
    $scope.display_name = Util.currentUserDisplayName();
    $scope.editing = false;
    $scope.isInputDisabled = true;
    $scope.profiles = null;
    $scope.selectedProfiles = [];
    $scope.allergens = [];
    $scope.enabledAllergens = [];
    $scope.allergensBtn = [];
    $scope.allergensDropdown = [];
    Allergens.get().$promise.then(function (data) {
        var allergensFromServer = data.allergens;
        angular.forEach(allergensFromServer, function (a) {
            if ((a.disable === null) || (a.disable == '0') || (a.disable === 0)) {
                $scope.allergens.push(a);
                $scope.enabledAllergens.push(a.allergen);
                $scope.allergensDropdown.push(a.allergen);
            }
        });

        angular.forEach($scope.enabledAllergens, function (a, i) {
            if (i <= 9) {
                $scope.allergensBtn.push(a);
            }
        });

        for (var key in $scope.allergensBtn) {
            var value = $scope.allergensBtn[key];
            var index = $scope.allergensDropdown.indexOf(value);
            if (index != -1) {
                $scope.allergensDropdown.splice(index, 1);
            }
        }
        $scope.refreshPage(); // init when page start
    }, function (error) {
        console.log(error);
    });

    // init
    $scope.refreshPage = function () { // get list of profiles
        // get profile from user
        console.log("current user id " + Util.currentUserId());
        $http.get(CONFIG.rootUrl + CONFIG.apiServer + '/user/' +Util.currentUserId() +'/profiles' ,{
            userId: Util.currentUserId()
        }).success(function (data) {
            $scope.profiles = angular.copy(data);
            // remove disabled data
            angular.forEach($scope.profiles, function (p) {
                var allergens = p.allergens;
                angular.forEach(allergens, function (e) {
                    if ($scope.enabledAllergens.indexOf(e) < 0) {
                        allergens.splice(allergens.indexOf(e), 1);
                    }
                });
            });
            if ($scope.profiles && $scope.profiles.length == 1) {
                $scope.profiles[0].checked = true;
                $scope.selectProfile($scope.profiles[0]);
                $scope.profileSelected = true;
            }
        }, function (data) {
            console.log("error occurred:", data);
        });
    };

    // scan
    // check if user is selected
    $scope.profileSelected = false;
    $scope.selectProfile = function (profile) {
        if (profile.checked) {
            $scope.selectedProfiles.push(profile); // select profile
        } else {
            $scope.selectedProfiles.splice($scope.selectedProfiles.indexOf(profile), 1); // deselect profile
        }

        if ($scope.selectedProfiles.length > 0) {
            $scope.profileSelected = true;
        } else {
            $scope.profileSelected = false;
        }
    };

    $scope.startScan = function () {
        $scope.matchedProfiles = [];
        $scope.closeAllResults();
        $scope.isDisabled = true;

        if ($scope.profileSelected) {
            $scope.openLoadingScreen();
            BarcodeScanner.scan(function (result) {
                if (result.cancelled) {
                    $timeout(function () {
                        $scope.isDisabled = false;
                        $scope.closeLoadingScreen();
                        $scope.closeAllResults();
                    }, 100);
                } else {
                    $scope.openLoadingScreen();
                    var url = encodeURIComponent('http://webservices.foodessentials.com/json/allergyvip/label.jsp?u=' + result.text + '&sid=9fe4492b-492e-4336-86b8-7d278e02aa51&n=10&s=0&f=json');
                    var api = 'http://foodallergywizard.com/api/api.php?page=' + url;
                    $http.get(api).success(function (data) {
                        console.log("url " + decodeURIComponent(url));
                        //console.log("data "+JSON.stringify(data));
                        $scope.product_name = data.product_name ? toTitleCase(data.product_name) : "No name";
                        $scope.product_warning = data.warnings;
                        if (data.Attributes || data.allergens) {
                            var selectedProfiles = angular.copy($scope.selectedProfiles); // list of selected profiles
                            var foundAllergens = []; // list of found allergens
                            var foundAllergensMayContain = []; // list of may contain allergens
                            var removeDuplicate = function (arr) {
                                var i,
									len = arr.length,
									out = [],
									obj = {};

                                for (i = 0; i < len; i++) {
                                    obj[arr[i]] = 0;
                                }
                                for (i in obj) {
                                    out.push(i);
                                }
                                return out;
                            }

                            // go through allergens list
                            for (var i = data.allergens.length - 1; i >= 0; i--) {
                                var allergen = data.allergens[i];
                                if (allergen.allergen_value == 2) {
                                    foundAllergens.push(allergen.allergen_name.toUpperCase());
                                }
                                if (allergen.allergen_value == 1) {
                                    foundAllergensMayContain.push(allergen.allergen_name.toUpperCase());
                                }
                            }

                            console.log("found allergens 1 " + foundAllergens);
                            console.log("found allergens may contain 1 " + foundAllergensMayContain);

                            // setup attributes list of product
                            for (var i = data.Attributes.length - 1; i >= 0; i--) {
                                var a = data.Attributes[i].replace('CONTAINS ', '');
                                if (foundAllergens.indexOf(a) == -1) {
                                    foundAllergens.push(a);
                                }
                            }

                            $scope.allergen_list = foundAllergens.join(", ");
                            $scope.allergen_list_may_contain = foundAllergensMayContain.join(", ")
                            console.log("found allergens 2 " + foundAllergens);
                            
                            // loop through each profile to find matched attributes
                            angular.forEach(selectedProfiles, function (p) {
                                var matched = false;
                                var allergens = [];
                                var foundIngredients = [];

                                for (var i = p.allergens.length - 1; i >= 0; i--) {
                                    allergens.push(p.allergens[i].toUpperCase());
                                }
                                if (allergens.indexOf("PEANUTS") > 0) {
                                    allergens.push("PEANUT");
                                }

                                console.log("matching allergens " + allergens);
                                //certainly contain these allergens.
                                for (var i = 0; i < foundAllergens.length; i++) {
                                    var a = foundAllergens[i];
                                    if (allergens.indexOf(a) > -1) {
                                        foundIngredients.push(a);
                                    }
                                }
                                //may contain these allergens.
                                for (var i = 0; i < foundAllergensMayContain.length; i++) {
                                    var a = foundAllergensMayContain[i];
                                    if (allergens.indexOf(a) > -1) {
                                        foundIngredients.push(a);
                                    }
                                }

                                console.log("warnings " + data.warnings);

                                for (var i = 0; i < allergens.length; i++) {
                                    if (data.warnings.indexOf(allergens[i]) > -1) {
                                        foundIngredients.push(allergens[i].toUpperCase());
                                    }
                                }

                                p.allergens = removeDuplicate(foundIngredients).join(', '); // clean up duplicates and join

                                if (p.allergens.length !== 0) {
                                    matched = true;
                                    $scope.matchedProfiles.push(p);
                                }
                            });

                            if ($scope.matchedProfiles.length > 0) {
                                $scope.openModalResultStop();
                                $timeout(function () {
                                    $scope.isDisabled = false;
                                }, 100);
                            } else {
                                $scope.openModalResultGood();
                                $timeout(function () {
                                    $scope.isDisabled = false;
                                }, 100);
                            }
                        } else {
                            $scope.openModalResultMaybe();
                            $timeout(function () {
                                $scope.isDisabled = false;
                            }, 100);
                        }
                    }).error(function (error) {
                        console.log(error);
                        $scope.openModalResultMaybe();
                        $timeout(function () {
                            $scope.isDisabled = false;
                        }, 100);
                    });
                }
            });
        } else {
            $ionicPopup.alert({
                title: 'No Profile Selected',
                template: 'You must select at least one profile',
                okType: 'button-royal'
            });
            $scope.isDisabled = false;
            $scope.closeAllResults();
        }
    };

    $scope.closeAllResults = function () {
        $scope.isDisabled = false;
        if ($scope.modalResultGood) {
            $scope.closeModalResultGood();
        }
        if ($scope.modalResultMaybe) {
            $scope.closeModalResultMaybe();
        }
        if ($scope.modalResultStop) {
            $scope.closeModalResultStop();
        }
        if ($scope.modalScanResults) {
            $scope.closeModalScanResults();
        }
        if ($scope.loadingScreen) {
            $scope.closeLoadingScreen();
        }
    };

    $scope.goToScanResults = function () {
        $scope.openModalScanResults();
        $scope.closeModalResultStop();
    };

    // Open our new task modal
    $scope.closeEdit = function () {
        $scope.closeModal();
    };

    $scope.editProfiles = function () {
        $scope.editing = true;
        $scope.profileSelected = false;
        angular.forEach($scope.profiles, function (p) {
            p.checked = false;
        });
    };

    $scope.deleteProfile = function (profile) {
        $http.get(CONFIG.rootUrl + CONFIG.apiServer + '/user/' +Util.currentUserId() +'/family_member/'+profile.id+'/delete')
            .success(function () {
            $scope.isDisabled = false;
            $scope.refreshPage(); // init when page start
        });
    };

    $scope.editProfile = function (profile) {
        $scope.profile = angular.copy(profile);
        $scope.openModal();
    };

    $scope.editUserPhoto = function () {
        // open modal to take/choose photo
        $scope.openModalSelectPhoto();
    };

    $scope.choosePhoto = function () {
        NativeCamera.getPicture({
            allowEdit: true,
            correctOrientation: true,
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
            targetHeight: 320,
            targetWidth: 320
        }).then(function (imageURI) {
            $scope.profile.image = 'data:image/jpg;base64,' + imageURI;
            $scope.openStep22();
        }, function (err) {
            console.err(err);
        });
    }

    $scope.takePhoto = function () {
        NativeCamera.getPicture({
            quality: 100,
            destinationType: Camera.DestinationType.DATA_URL,
            allowEdit: true,
            targetWidth: 320,
            targetHeight: 320,
            saveToPhotoAlbum: false
        }).then(function (imageURI) {
            $scope.profile.image = 'data:image/jpg;base64,' + imageURI;
            $scope.openStep23();
        }, function (err) {
            console.err(err);
        });
    }

    $scope.backToStep1 = function () {
        $scope.closeStep22();
        $scope.closeStep23();
    }

    $scope.setNewPhoto = function () {
        $scope.closeModalSelectPhoto();
        if ($scope.modalStep22) {
            $scope.closeStep22();
        } else {
            $scope.closeStep23();
        }
    };

    $scope.addAllergen = function () {
        $scope.newAllernModal.show();
    };

    $scope.toggleAllergen = function (allergen) {
        var idx = $scope.profile.allergens.indexOf(allergen);
        if (idx > -1) { // is currently selected
            $scope.profile.allergens.splice(idx, 1);
        } else { // is newly selected
            $scope.profile.allergens.push(allergen);
            angular.forEach($scope.allergens, function (a) {
                if (a.allergen == allergen) {
                    if (a.related && $scope.profile.allergens.indexOf(a.related) < 0 && $scope.enabledAllergens.indexOf(a.related) > 0) {
                        $scope.selectedAllergen = a.allergen;
                        $scope.relatedAllergen = a.related;
                        $scope.openModalNewAllergen();
                    }
                }
            });
        }
    };

    $scope.addMoreAllergen = function (relatedAllergen) {
        // push the allergen list
        $scope.profile.allergens.push(relatedAllergen);
        //reset
        $scope.selectedAllergen = '';
        $scope.relatedAllergen = '';
        $scope.closeModalNewAllergen(); // close modal
    };

    $scope.seeMoreAllergens = function () {
        $scope.openModalMoreAllergens();
    }

    $scope.cancelAddAllergen = function () {
        $scope.closeModalNewAllergen(); // close modal
    };

    $scope.doneEdit = function () {
        $scope.editing = false;
        $scope.selectedProfiles = []; // reset selected list
        $scope.refreshPage(); //refresh list profiles
    };

    $scope.saveProfile = function (info) {
        $scope.openModalConfirm();
    }

    $scope.closeConfirmModal = function () {
        $scope.closeModalConfirm();
    };

    $scope.confirmProfile = function () {
        if ($scope.editing) {
            var data_to_send = JSON.stringify({
                "profile_info": {
                    allergens: $scope.profile.allergens,
                    firstname: $scope.profile.firstname,
                    lastname: $scope.profile.lastname,
                    image: $scope.profile.image
                }
            });

            $scope.isDisabled = true;
            $http.post(CONFIG.rootUrl + CONFIG.apiServer + '/user/' +Util.currentUserId() +'/family_member/'+$scope.profile.id+'/edit',data_to_send)
                .success(function () {
                $scope.isDisabled = false;
                $scope.closeModalConfirm();
                $scope.closeModal();
                $scope.refreshPage();
            }, function () {
                $scope.isDisabled = false;
            })
        }
    };

    $scope.$on('modal.shown', function () {
        setTimeout(function () {
            $scope.isInputDisabled = false;
        }, 100);
    });

    $scope.$on('modal.removed', function () {
        $scope.isInputDisabled = true;
    });

    // Create and load the Modal
    var initModal = function () {
        if ($scope.modal) {
            return $q.when();
        } else {
            return $ionicModal.fromTemplateUrl('edit-modal.html', {
                scope: $scope,
                animation: 'slide-in-right'
            }).then(function (modal) {
                $scope.modal = modal;
            });
        }
    };
    $scope.openModal = function () {
        initModal().then(function () {
            $scope.modal.show();
        });
    };
    $scope.closeModal = function () {
        $scope.modal.remove().then(function () {
            $scope.modal = null;
        });
    };

    var initModalMoreAllergens = function () {
        if ($scope.modalMoreAllergens) {
            return $q.when();
        } else {
            return $ionicModal.fromTemplateUrl('more-allergens-modal.html', {
                scope: $scope,
                animation: 'slide-in-right'
            }).then(function (modal) {
                $scope.modalMoreAllergens = modal;
            });
        }
    };
    $scope.openModalMoreAllergens = function () {
        initModalMoreAllergens().then(function () {
            $scope.modalMoreAllergens.show();
        });
    };
    $scope.closeModalMoreAllergens = function () {
        $scope.modalMoreAllergens.remove().then(function () {
            $scope.modalMoreAllergens = null;
        });
    };

    var initModalConfirm = function () {
        if ($scope.modalConfirm) {
            return $q.when();
        } else {
            return $ionicModal.fromTemplateUrl('confirm-modal.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal) {
                $scope.modalConfirm = modal;
            });
        }
    };
    $scope.openModalConfirm = function () {
        initModalConfirm().then(function () {
            $scope.modalConfirm.show();
        });
    };
    $scope.closeModalConfirm = function () {
        $scope.modalConfirm.remove().then(function () {
            $scope.modalConfirm = null;
        });
    };

    var initModalNewAllergen = function () {
        if ($scope.modalNewAllergen) {
            return $q.when();
        } else {
            return $ionicModal.fromTemplateUrl('new-allergen-modal.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal) {
                $scope.modalNewAllergen = modal;
            });
        }
    };
    $scope.openModalNewAllergen = function () {
        initModalNewAllergen().then(function () {
            $scope.modalNewAllergen.show();
        });
    };
    $scope.closeModalNewAllergen = function () {
        $scope.modalNewAllergen.remove().then(function () {
            $scope.modalNewAllergen = null;
        });
    };

    var initModalResultGood = function () {
        if ($scope.modalResultGood) {
            return $q.when();
        } else {
            return $ionicModal.fromTemplateUrl('result-good.html', {
                scope: $scope,
                animation: 'slide-in-right'
            }).then(function (modal) {
                $scope.modalResultGood = modal;
            });
        }
    };
    $scope.openModalResultGood = function () {
        initModalResultGood().then(function () {
            $scope.modalResultGood.show();
        });
    };
    $scope.closeModalResultGood = function () {
        $scope.modalResultGood.remove().then(function () {
            $scope.modalResultGood = null;
        });
    };

    var initModalResultMaybe = function () {
        if ($scope.modalResultMaybe) {
            return $q.when();
        } else {
            return $ionicModal.fromTemplateUrl('result-maybe.html', {
                scope: $scope,
                animation: 'slide-in-right'
            }).then(function (modal) {
                $scope.modalResultMaybe = modal;
            });
        }
    };
    $scope.openModalResultMaybe = function () {
        initModalResultMaybe().then(function () {
            $scope.modalResultMaybe.show();
        });
    };
    $scope.closeModalResultMaybe = function () {
        $scope.modalResultMaybe.remove().then(function () {
            $scope.modalResultMaybe = null;
        });
    };

    var initModalResultStop = function () {
        if ($scope.modalResultStop) {
            return $q.when();
        } else {
            return $ionicModal.fromTemplateUrl('result-stop.html', {
                scope: $scope,
                animation: 'slide-in-right'
            }).then(function (modal) {
                $scope.modalResultStop = modal;
            });
        }
    };
    $scope.openModalResultStop = function () {
        initModalResultStop().then(function () {
            $scope.modalResultStop.show();
        });
    };
    $scope.closeModalResultStop = function () {
        $scope.modalResultStop.remove().then(function () {
            $scope.modalResultStop = null;
        });
    };

    var initModalScanResults = function () {
        if ($scope.modalScanResults) {
            return $q.when();
        } else {
            return $ionicModal.fromTemplateUrl('results.html', {
                scope: $scope,
                animation: 'slide-in-right'
            }).then(function (modal) {
                $scope.modalScanResults = modal;
            });
        }
    };
    $scope.openModalScanResults = function () {
        initModalScanResults().then(function () {
            $scope.modalScanResults.show();
        });
    };
    $scope.closeModalScanResults = function () {
        $scope.modalScanResults.remove().then(function () {
            $scope.modalScanResults = null;
        });
    };

    var initModalSelectPhoto = function () {
        if ($scope.modalSelectPhoto) {
            return $q.when();
        } else {
            return $ionicModal.fromTemplateUrl('select-photo.html', {
                scope: $scope,
                animation: 'slide-in-right'
            }).then(function (modal) {
                $scope.modalSelectPhoto = modal;
            });
        }
    };
    $scope.openModalSelectPhoto = function () {
        initModalSelectPhoto().then(function () {
            $scope.modalSelectPhoto.show();
        });
    };
    $scope.closeModalSelectPhoto = function () {
        $scope.modalSelectPhoto.remove().then(function () {
            $scope.modalSelectPhoto = null;
        });
    };

    var initStep22 = function () {
        if ($scope.modalStep22) {
            return $q.when();
        } else {
            return $ionicModal.fromTemplateUrl('step22.html', {
                scope: $scope,
                animation: 'slide-in-right'
            }).then(function (modal) {
                $scope.modalStep22 = modal;
            });
        }
    };
    $scope.openStep22 = function () {
        initStep22().then(function () {
            $scope.modalStep22.show();
        });
    };
    $scope.closeStep22 = function () {
        $scope.modalStep22.remove().then(function () {
            $scope.modalStep22 = null;
        });
    };

    var initStep23 = function () {
        if ($scope.modalStep23) {
            return $q.when();
        } else {
            return $ionicModal.fromTemplateUrl('step23.html', {
                scope: $scope,
                animation: 'slide-in-right'
            }).then(function (modal) {
                $scope.modalStep23 = modal;
            });
        }
    };
    $scope.openStep23 = function () {
        initStep23().then(function () {
            $scope.modalStep23.show();
        });
    };
    $scope.closeStep23 = function () {
        $scope.modalStep23.remove().then(function () {
            $scope.modalStep23 = null;
        });
    };

    var initLoading = function () {
        if ($scope.loadingScreen) {
            return $q.when();
        } else {
            return $ionicModal.fromTemplateUrl('loading.html', {
                scope: $scope,
                animation: 'slide-in-right'
            }).then(function (modal) {
                $scope.loadingScreen = modal;
            });
        }
    };
    $scope.openLoadingScreen = function () {
        initLoading().then(function () {
            $scope.loadingScreen.show();
        });
    };
    $scope.closeLoadingScreen = function () {
        $scope.loadingScreen.remove().then(function () {
            $scope.loadingScreen = null;
        });
    };

    $scope.createNewProfile = function () {
        $location.path('/create-profile');
    };
});

function toTitleCase(str) {
    return str.replace(/\w\S*/g, function (txt) { return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); });
}