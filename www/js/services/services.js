angular.module('FoodAllergyWizard.services', [])

.factory('Util', function() {
	return {	
		currentUserId: function() {
			return window.localStorage['current_user_id'];
		},
		setCurrentUserId: function(s) {
			window.localStorage['current_user_id'] = s;
			if (window.cordova) {
				plugins.appPreferences.store(function(){}, function(){}, 'current_user_id', s);
			}
		},
		currentUserDisplayName: function() {
			return window.localStorage['current_user_display_name'];
		},
	}
})

.factory('BarcodeScanner', [ '$ionicPopup', '$rootScope',
	function($ionicPopup, $rootScope) {
		return {
			scan: function(success, fail) {
				if (false) {
					success({text: "044000026967"}); // nabisco cookies
					//success({text: "644209311323"}); // duncan brownies
					//success({text: "632565000029"}); // fiji water
				} else if (ionic.Platform.platforms.indexOf("browser") != -1) {
				  var $scope = $rootScope.$new(true);
				  $scope.data = {}
		
				  $ionicPopup.show({
					template: '<input type="text" ng-model="data.upc">',
					title: 'Enter UPC code',
					scope: $scope,
					buttons: [
					  { text: 'Cancel' },
					  {
						text: '<b>Save</b>',
						type: 'button-positive',
						onTap: function(e) {
						  if (!$scope.data.upc) {
							e.preventDefault();
						  } else {
							return $scope.data.upc;
						  }
						}
					  }
					]
				  }).then(function(res) {
					success({text: $scope.data.upc});
				  });
				} else {
				  cordova.plugins.barcodeScanner.scan(
					function(result) {
					  success(result);
					},
					function(error) {
					  fail(error);
					}
				  );
				}
			}
		};
	}
])

.factory('NativeCamera', ['$q',
	function($q) {
		return {
			getPicture: function(options) {
				var q = $q.defer();

				if (!navigator.camera) {
					q.resolve(null);
					return q.promise;
				}

				navigator.camera.getPicture(function(imageData) {
					q.resolve(imageData);
				}, function(err) {
					q.reject(err);
				}, options);

				return q.promise;
			},

			cleanup: function(options) {
				var q = $q.defer();
				navigator.camera.cleanup(function() {
					q.resolve(arguments);
				}, function(err) {
					q.reject(err);
				});

				return q.promise;
			}
		}
	}
])

.factory('SocialSharing', ['$q',
	function($q) {
		return {
			share: function(message, subject, file, link) {
				var q = $q.defer();
				window.plugins.socialsharing.share(message, subject, file, link,
					function() {
						q.resolve(true); // success
					},
					function() {
						q.reject(false); // error
					});
				return q.promise;
			},

			shareViaTwitter: function(message, file, link) {
				var q = $q.defer();
				window.plugins.socialsharing.shareViaTwitter(message, file, link,
					function() {
						q.resolve(true); // success
					},
					function() {
						q.reject(false); // error
					});
				return q.promise;
			},

			shareViaWhatsApp: function(message, file, link) {
				var q = $q.defer();
				window.plugins.socialsharing.shareViaWhatsApp(message, file, link,
					function() {
						q.resolve(true); // success
					},
					function() {
						q.reject(false); // error
					});
				return q.promise;
			},

			shareViaFacebook: function(message, file, link) {
				var q = $q.defer();
				window.plugins.socialsharing.shareViaFacebook(message, file, link,
					function() {
						q.resolve(true); // success
					},
					function() {
						q.reject(false); // error
					});
				return q.promise;
			},

			shareViaSMS: function(message, commaSeparatedPhoneNumbers) {
				var q = $q.defer();
				window.plugins.socialsharing.shareViaSMS(message, commaSeparatedPhoneNumbers,
					function() {
						q.resolve(true); // success
					},
					function() {
						q.reject(false); // error
					});
				return q.promise;
			},

			shareViaEmail: function(message, subject, toArr, ccArr, bccArr, fileArr) {
				var q = $q.defer();
				window.plugins.socialsharing.shareViaEmail(message, subject, toArr, ccArr, bccArr, fileArr,
					function() {
						q.resolve(true); // success
					},
					function() {
						q.reject(false); // error
					});
				return q.promise;
			},

			canShareViaEmail: function() {
				var q = $q.defer();
				window.plugins.socialsharing.canShareViaEmail(
					function() {
						q.resolve(true); // success
					},
					function() {
						q.reject(false); // error
					});
				return q.promise;
			},

			canShareVia: function(via, message, subject, file, link) {
				var q = $q.defer();
				window.plugins.socialsharing.canShareVia(via, message, subject, file, link,
					function(success) {
						q.resolve(success); // success
					},
					function(error) {
						q.reject(error); // error
					});
				return q.promise;
			},

			shareVia: function(via, message, subject, file, link) {
				var q = $q.defer();
				window.plugins.socialsharing.shareVia(via, message, subject, file, link,
					function() {
						q.resolve(true); // success
					},
					function() {
						q.reject(false); // error
					});
				return q.promise;
			}

		}
	}
]);
