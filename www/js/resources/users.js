angular.module('FoodAllergyWizard.Resource.Users', [
	'ngResource'
])

.factory('Users', ['$resource', '$cacheFactory', 'CONFIG',
	function($resource, $cacheFactory, Config) {
		var apiServer = Config.rootUrl + Config.apiServer;
		var r = $resource(apiServer + '/user/:listPath:userId/:docPath/:familyId/:docPath2/:docPath3', {
			userId: '@id',
			familyId: '@familyId'
		}, {



		});
		return r;
	}
]);