angular.module('FoodAllergyWizard.Resource.Allergens', [
	'ngResource'
])

.factory('Allergens', ['$resource', '$cacheFactory', 'CONFIG',
	function($resource, $cacheFactory, Config) {
		var apiServer = Config.rootUrl + Config.apiServer;
		var r = $resource(apiServer + '/allergens/get', {});
		return r;
	}
])
